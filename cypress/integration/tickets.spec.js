describe("Tickets", () => {
  beforeEach(() => {
    cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html");
  });

  it("Prenche os campos do tipo texto", () => {
    const firstName = "Tarcisio";
    const lastName = "Uchoa";

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("cisoxp@gmail.com");
    cy.get("#requests").type("Lorem ipsum dolor sit amet");
    cy.get("#signature").type(firstName + " " + lastName);
  });

  it("Selecionar 2 tickets", () => {
    cy.get("#ticket-quantity").select("2");
  });

  it("Selecionar tipo de ticket VIP", () => {
    cy.get("#vip").check();
  });

  it('Marcar o checkbox "social media"', () => {
    cy.get("#social-media").check();
  });
  it('Marcar o checkbox "friend" e "publication", depois desmarcar "friend"', () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#friend").uncheck();
  });
  it('Verificar o título "ticketbox"', () => {
    cy.get("header h1").should("contain", "TICKETBOX");
  });

  // verificar se a validação mudou a classe do campo
  it("Alerta e-mail inválido", () => {
    cy.get("#email").type("teste.com");
    cy.get("#email.invalid").should("exist");
  });

  //melhorando o código
  it("Alerta e-mail inválido", () => {
    cy.get("#email").as("email").type("teste.com"); // dar apelido
    cy.get("#email.invalid").should("exist");

    cy.get("@email").clear().type("cisoxp@gmail.com"); // utilizar o apelido com @
    cy.get("#email.invalid").should("not.exist");
  });

  it("Preencher e resetar o formulário", () => {
    const firstName = "Tarcisio";
    const lastName = "Uchoa";
    const fullName = firstName + " " + lastName;

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("cisoxp@gmail.com");
    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check();
    cy.get("#friend").check();
    cy.get("#requests").type("Lorem ipsum dolor sit amet");
    cy.get(".agreement p").should(
      "contain",
      `I, ${fullName}, wish to buy 2 VIP tickets`
    );
    cy.get("#agree").click();
    cy.get("#signature").type(fullName);

    cy.get("button[type=submit]").as("submitButton").should("not.be.disabled");
    cy.get("button[type=reset]").click();
    cy.get("@submitButton").should("be.disabled");
  });

  it("Preencher apenas campos obrigatórios com comando de suporte", () => {
    const customer = {
      firstName: "Tarcisio",
      lastName: "Uchoa",
      email: "cisoxp@gmail.com",
    };

    cy.fillMantadoryFields(customer);

    cy.get("button[type=submit]").as("submitButton").should("not.be.disabled");
    cy.get("#agree").uncheck();
    cy.get("@submitButton").should("be.disabled");
  });
});
